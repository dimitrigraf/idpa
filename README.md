https://timofurrer.github.io/idpa

Diese IDPA (Interdisziplinäre Projektarbeit) wurde von Timo Furrer und mir, Dimitri Graf, im Rahmen unserer Informatiker-Ausbildung verfasst. Die Arbeit wurde ursprünglich in einem Github-Repo von Timo Furrer gehostet. Nachdem ich meine Projekte von Github zu Gitlab migriert hatte, klonte ich auch dieses Repo, damit es sicherlich nicht verloren geht. 